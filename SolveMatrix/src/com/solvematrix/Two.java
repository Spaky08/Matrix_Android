package com.solvematrix;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Two extends Activity implements OnClickListener {
	private int fils=2,cols=2;
	private EditText [][]b=new EditText[fils][cols];
	private Matrix solver=new Matrix(fils,cols);
	private EditText Dt;
	private Button det,ind;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.two);
		b[0][0]=(EditText)findViewById(R.id.EditText1);
		b[0][1]=(EditText)findViewById(R.id.EditText2);
		b[1][0]=(EditText)findViewById(R.id.EditText3);
		b[1][1]=(EditText)findViewById(R.id.EditText4);
		Dt=(EditText)findViewById(R.id.editText5);
		det=(Button)findViewById(R.id.DET_2);
		ind=(Button)findViewById(R.id.ide2);
		det.setOnClickListener(this);
		ind.setOnClickListener(this);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.two, menu);
		return true;
	}
	protected double getVal(EditText a){
		double val=0;
		try{
			String x=a.getText().toString();
			if(x=="")return 0;
			val=Double.parseDouble(x);
		}catch(Exception ex){
			val=0;
		}
		return val;
		
	}	
	protected void getValues(){
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
				solver.setMatrix(i, j,getVal(b[i][j]));
				
	}
	protected void setIndenty(){
		solver.Identity();
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
					b[i][j].setText(String.valueOf(solver.getMatrix(i,j)));
	}
	@Override
	public void onClick(View v) {
		getValues();
		switch(v.getId()){
		case R.id.DET_2:{
			//Determinante
			double x=solver.getDet();
			Dt.setText(String.valueOf(x));
			break;
		}
		case R.id.ide2:{
			//identidad
			setIndenty();
			Dt.setText("");
			break;
		}
		
		
		}
		
	}

}
