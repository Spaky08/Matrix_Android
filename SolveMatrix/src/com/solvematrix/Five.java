package com.solvematrix;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Five extends Activity implements OnClickListener{
	private int fils=5,cols=5;
	private EditText [][]b=new EditText[fils][cols];
	private Matrix solver=new Matrix(fils,cols);
	private EditText Dt;
	private Button det,ind;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_five);
		
		b[0][0]=(EditText)findViewById(R.id.E1_1);
		b[0][1]=(EditText)findViewById(R.id.E2_1);
		b[0][2]=(EditText)findViewById(R.id.E3_1);
		b[0][3]=(EditText)findViewById(R.id.E4_1);
		b[0][4]=(EditText)findViewById(R.id.E5_1);
		
		b[1][0]=(EditText)findViewById(R.id.E6_1);
		b[1][1]=(EditText)findViewById(R.id.E7_1);
		b[1][2]=(EditText)findViewById(R.id.E8_1);
		b[1][3]=(EditText)findViewById(R.id.E9_1);
		b[1][4]=(EditText)findViewById(R.id.E10_1);
		
		b[2][0]=(EditText)findViewById(R.id.E11_1);
		b[2][1]=(EditText)findViewById(R.id.E12_1);
		b[2][2]=(EditText)findViewById(R.id.E13_1);
		b[2][3]=(EditText)findViewById(R.id.E14_1);
		b[2][4]=(EditText)findViewById(R.id.E15_1);
		
		b[3][0]=(EditText)findViewById(R.id.E16_1);
		b[3][1]=(EditText)findViewById(R.id.E17_1);
		b[3][2]=(EditText)findViewById(R.id.E18_1);
		b[3][3]=(EditText)findViewById(R.id.E19_1);
		b[3][4]=(EditText)findViewById(R.id.E20_1);
		
		b[4][0]=(EditText)findViewById(R.id.E21_1);
		b[4][1]=(EditText)findViewById(R.id.E22_1);
		b[4][2]=(EditText)findViewById(R.id.E23_1);
		b[4][3]=(EditText)findViewById(R.id.E24_1);
		b[4][4]=(EditText)findViewById(R.id.E25_1);
		
		Dt=(EditText)findViewById(R.id.E27_1);
		det=(Button)findViewById(R.id.DET_5);
		ind=(Button)findViewById(R.id.ide5_);
		det.setOnClickListener(this);
		ind.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.five, menu);
		return true;
	}

	protected double getVal(EditText a){
		double val=0;
		try{
			String x=a.getText().toString();
			if(x=="")return 0;
			val=Double.parseDouble(x);
		}catch(Exception ex){
			val=0;
		}
		return val;	
	}	
	protected void getValues(){
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
				solver.setMatrix(i, j,getVal(b[i][j]));
	}
	protected void setIndenty(){
		solver.Identity();
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
					b[i][j].setText(String.valueOf(solver.getMatrix(i,j)));
	}
	@Override
	public void onClick(View v) {
		getValues();
		switch(v.getId()){
		case R.id.DET_5:{
			//Determinante
			double x=solver.getDet();
			Dt.setText(String.valueOf(x));
			break;
		}
		case R.id.ide5_:{
			//identidad
			setIndenty();
			Dt.setText("");
			break;
		}
		}	
	}
}
