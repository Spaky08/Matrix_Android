package com.solvematrix;
//Clase para resolver matriz
public class Matrix {
	private double [][]M;
	private int fils,cols;
	public Matrix(int fils,int cols){
		this.fils=fils;
		this.cols=cols;	
		M=new double[fils][cols];
	}
	
	public void setMatrix(int i , int j, double x ){
		M[i][j]=x;
	}
	public double getMatrix(int i ,int j){
		return M[i][j];
	}
	//Calcula la matriz identidad
	public void Identity(){
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++){
				if(i==j){
					M[i][j]=1;
				}else{
					M[i][j]=0;	
				}
			}			
	}
	//obtiene la Determinante
	public double getDet(){
		return (determinante(this.M));
	}
	/*
	 * Calcula la Determinante
	 * */
	public double determinante(double[][] matriz){
	    double det;
	    if(matriz.length==2){
	        det=(matriz[0][0]*matriz[1][1])-(matriz[1][0]*matriz[0][1]);
	        return det;
	    }
	    double suma=0;
	    for(int i=0; i<matriz.length; i++){
	    double[][] nm=new double[matriz.length-1][matriz.length-1];
	        for(int j=0; j<matriz.length; j++){
	            if(j!=i){
	                for(int k=1; k<matriz.length; k++){
	                int indice=-1;
	                if(j<i)
	                indice=j;
	                else if(j>i)
	                indice=j-1;
	                nm[indice][k-1]=matriz[j][k];
	                }
	            }
	        }
	        if(i%2==0)
	        	suma+=matriz[i][0] * determinante(nm);
	        else
	        	suma-=matriz[i][0] * determinante(nm);
	    }
	    return suma;
	}
}
