package com.solvematrix;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Four extends Activity implements OnClickListener{
	private int fils=4,cols=4;
	private EditText [][]b=new EditText[fils][cols];
	private Matrix solver=new Matrix(fils,cols);
	private EditText Dt;
	private Button det,ind;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	
		setContentView(R.layout.activity_four);
		b[0][0]=(EditText)findViewById(R.id.E1_);
		b[0][1]=(EditText)findViewById(R.id.E2_);
		b[0][2]=(EditText)findViewById(R.id.E3_);
		b[0][3]=(EditText)findViewById(R.id.E4_);
		b[1][0]=(EditText)findViewById(R.id.E5_);
		b[1][1]=(EditText)findViewById(R.id.E6_);
		b[1][2]=(EditText)findViewById(R.id.E7_);
		b[1][3]=(EditText)findViewById(R.id.E8_);
		
		b[2][0]=(EditText)findViewById(R.id.E9_);
		b[2][1]=(EditText)findViewById(R.id.E10_);
		b[2][2]=(EditText)findViewById(R.id.E11_);
		b[2][3]=(EditText)findViewById(R.id.E12_);
		
		b[3][0]=(EditText)findViewById(R.id.E13_);
		b[3][1]=(EditText)findViewById(R.id.E14_);
		b[3][2]=(EditText)findViewById(R.id.E15_);
		b[3][3]=(EditText)findViewById(R.id.E16_);
		Dt=(EditText)findViewById(R.id.E26_1);
		det=(Button)findViewById(R.id.DET_4);
		ind=(Button)findViewById(R.id.ide4);
		det.setOnClickListener(this);
		ind.setOnClickListener(this);
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.four, menu);
		return true;
	}
	protected double getVal(EditText a){
		double val=0;
		try{
			String x=a.getText().toString();
			if(x=="")return 0;
			val=Double.parseDouble(x);
		}catch(Exception ex){
			val=0;
		}
		return val;	
	}	
	protected void getValues(){
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
				solver.setMatrix(i, j,getVal(b[i][j]));
				
	}
	protected void setIndenty(){
		solver.Identity();
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
					b[i][j].setText(String.valueOf(solver.getMatrix(i,j)));
	}
	@Override
	public void onClick(View v) {
		getValues();
		switch(v.getId()){
		case R.id.DET_4:{
			//Determinante
			double x=solver.getDet();
			Dt.setText(String.valueOf(x));
			break;
		}
		case R.id.ide4:{
			//identidad
			setIndenty();
			Dt.setText("");
			break;
		}
		
		
		}
		
	}

}
