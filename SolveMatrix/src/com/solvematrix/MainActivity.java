package com.solvematrix;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {
	private Button one,two,there,four;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		one=(Button)findViewById(R.id.DET_2);
		two=(Button)findViewById(R.id.Button2);
		there=(Button)findViewById(R.id.Button3);
		four=(Button)findViewById(R.id.five);
		one.setOnClickListener(this);
		two.setOnClickListener(this);
		there.setOnClickListener(this);
		four.setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		Intent a;
		switch(v.getId()){
		case R.id.DET_2:{
			a=new Intent(this,Two.class);
		startActivity(a);
		break;

		}
		case R.id.Button2:{
			a=new Intent(this,There.class);
		startActivity(a);
		break;
			
		}
		
		case R.id.Button3:{
			a=new Intent(this,Four.class);
		startActivity(a);
		break;
			
		}
		case R.id.five:{
			a=new Intent(this,Five.class);
		startActivity(a);
		break;
			
		}
		
		
		}
		// TODO Auto-generated method stub
		
	}

}
