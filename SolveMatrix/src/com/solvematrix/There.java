package com.solvematrix;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class There extends Activity  implements OnClickListener{
	private int fils=3,cols=3;
	private EditText [][]b=new EditText[fils][cols];
	private Matrix solver=new Matrix(fils,cols);
	private EditText Dt;
	private Button det,ind;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_there);
		b[0][0]=(EditText)findViewById(R.id.E1);
		b[0][1]=(EditText)findViewById(R.id.E2);
		b[0][2]=(EditText)findViewById(R.id.E3);
		
		b[1][0]=(EditText)findViewById(R.id.E4);
		b[1][1]=(EditText)findViewById(R.id.E5);
		b[1][2]=(EditText)findViewById(R.id.E6);
		
		b[2][0]=(EditText)findViewById(R.id.E7);
		b[2][1]=(EditText)findViewById(R.id.E8);
		b[2][2]=(EditText)findViewById(R.id.E9);
		Dt=(EditText)findViewById(R.id.E10);
		det=(Button)findViewById(R.id.DET_3);
		ind=(Button)findViewById(R.id.ide3);
		det.setOnClickListener(this);
		ind.setOnClickListener(this);
	}
	protected double getVal(EditText a){
		double val=0;
		try{
			String x=a.getText().toString();
			if(x=="")return 0;
			val=Double.parseDouble(x);
		}catch(Exception ex){
			val=0;
		}
		return val;	
	}	
	protected void getValues(){
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
				solver.setMatrix(i, j,getVal(b[i][j]));
				
	}
	protected void setIndenty(){
		solver.Identity();
		for(int i=0;i<this.fils;i++)
			for(int j=0;j<this.cols;j++)
					b[i][j].setText(String.valueOf(solver.getMatrix(i,j)));
	}
	@Override
	public void onClick(View v) {
		getValues();
		switch(v.getId()){
		case R.id.DET_3:{
			//Determinante
			double x=solver.getDet();
			Dt.setText(String.valueOf(x));
			break;
		}
		case R.id.ide3:{
			//identidad
			setIndenty();
			Dt.setText("");
			break;
		}
		
		
		}
		
	}
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.there, menu);
		return true;
	}

}
